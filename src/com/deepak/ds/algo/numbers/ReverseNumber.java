package com.deepak.ds.algo.numbers;

public class ReverseNumber {
    public static void main(String[] args) {
        reverseNumber(12345);
    }

    private static void reverseNumber(int num) {
        int mul = (int) Math.pow(10, countDigit(num) - 1);
        int reverse = 0;
        while (num > 0) {
            reverse = reverse + (mul * (num % 10));
            num = num / 10;
            mul = mul / 10;
        }
        System.out.println(reverse);
    }

    private static int countDigit(int num) {
        int count = 0;
        while (num > 0) {
            num = num / 10;
            count++;
        }
        return count;
    }
}
