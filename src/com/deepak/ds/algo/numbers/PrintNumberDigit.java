package com.deepak.ds.algo.numbers;

import static java.lang.System.*;

public class PrintNumberDigit {
    public static void main(String[] args) {
        printDigit(123945);
    }

    private static void printDigit(int num) {
        int div = (int) Math.pow(10,countDigit(num)-1);
        while (num>0){
            out.println(num/div);
            num = num%div;
            div = div/10;
        }
    }

    private static int countDigit(int num) {
        int count = 0;
        while (num > 0){
            num = num/10;
            count++;
        }
        return count;
    }
}
