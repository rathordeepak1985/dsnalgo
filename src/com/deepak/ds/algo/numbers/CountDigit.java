package com.deepak.ds.algo.numbers;

import static java.lang.System.*;

public class CountDigit {
	public static void main(String[] args) {
		out.println(countDigit(45));
	}

	private static int countDigit(int num) {
		int count = 0;
		while (num > 0){
			num = num/10;
			count++;
		}
		return count;
	}
}
