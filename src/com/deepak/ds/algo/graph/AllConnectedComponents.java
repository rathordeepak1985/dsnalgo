package com.deepak.ds.algo.graph;

import java.util.ArrayList;
import java.util.List;

public class AllConnectedComponents {

	public static void main(String[] args) {
		ArrayList<Edge>[] graph = createGraph();
		System.out.println(findAllConnectedComponents(graph));
		System.out.println(isConnected(graph));

		ArrayList<Edge>[] graph2 = createGraph2();
		System.out.println(findAllConnectedComponents(graph2));
		System.out.println(isConnected(graph2));
	}

	private static boolean isConnected(ArrayList<Edge>[] graph) {
		boolean[] visited = new boolean[graph.length];
		List<List<Integer>> components = new ArrayList<>();

		for (int v = 0; v < visited.length; v++) {
			if (!visited[v]) {
				List<Integer> path = new ArrayList<>();
				dfs(graph, visited, v, path);
				components.add(path);
			}
		}
		return components.size() == 1;
	}

	private static List<List<Integer>> findAllConnectedComponents(ArrayList<Edge>[] graph) {
		boolean[] visited = new boolean[graph.length];
		List<List<Integer>> components = new ArrayList<>();

		for (int v = 0; v < visited.length; v++) {
			if (!visited[v]) {
				List<Integer> path = new ArrayList<>();
				dfs(graph, visited, v, path);
				components.add(path);
			}
		}
		return components;
	}

	private static void dfs(ArrayList<Edge>[] graph, boolean[] visited, int v, List<Integer> path) {
		path.add(v);
		visited[v] = true;

		for (Edge edge : graph[v]) {
			if (!visited[edge.dst])
				dfs(graph, visited, edge.dst, path);
		}
	}

	@SuppressWarnings("unchecked")
	private static ArrayList<Edge>[] createGraph() {
		ArrayList<Edge>[] graph = new ArrayList[7];

		for (int i = 0; i < graph.length; i++) {
			graph[i] = new ArrayList<>();
		}

		graph[0].add(new Edge(0, 1, 10));

		graph[1].add(new Edge(1, 0, 10));

		graph[2].add(new Edge(2, 3, 10));

		graph[3].add(new Edge(3, 2, 10));

		graph[4].add(new Edge(4, 5, 3));
		graph[4].add(new Edge(4, 6, 8));

		graph[5].add(new Edge(5, 4, 3));
		graph[5].add(new Edge(5, 6, 3));

		graph[6].add(new Edge(6, 4, 8));
		graph[6].add(new Edge(6, 5, 3));

		return graph;
	}

	@SuppressWarnings("unchecked")
	private static ArrayList<Edge>[] createGraph2() {
		ArrayList<Edge>[] graph = new ArrayList[7];

		for (int i = 0; i < graph.length; i++) {
			graph[i] = new ArrayList<>();
		}

		graph[0].add(new Edge(0, 1, 10));
		graph[0].add(new Edge(0, 3, 40));

		graph[1].add(new Edge(1, 0, 10));
		graph[1].add(new Edge(1, 2, 10));

		graph[2].add(new Edge(2, 1, 10));
		graph[2].add(new Edge(2, 3, 10));

		graph[3].add(new Edge(3, 0, 40));
		graph[3].add(new Edge(3, 2, 10));
		graph[3].add(new Edge(3, 4, 2));

		graph[4].add(new Edge(4, 3, 2));
		graph[4].add(new Edge(4, 5, 3));
		graph[4].add(new Edge(4, 6, 8));

		graph[5].add(new Edge(5, 4, 3));
		graph[5].add(new Edge(5, 6, 3));

		graph[6].add(new Edge(6, 4, 8));
		graph[6].add(new Edge(6, 5, 3));

		return graph;
	}

	static class Edge {
		int src;
		int dst;
		int weight;

		public Edge(int src, int dst, int weight) {
			super();
			this.src = src;
			this.dst = dst;
			this.weight = weight;
		}

		@Override
		public String toString() {
			return src + "-" + dst + "@" + weight;
		}
	}

}
