package com.deepak.ds.algo.graph;

import java.util.ArrayList;
import java.util.Arrays;

public class FindAllPaths {
	public static void main(String[] args) {
		ArrayList<Edge>[] graph = createGraph();
		System.out.println(Arrays.toString(graph));
		findAllPaths(graph, 0, 6, new boolean[7], "");
	}

	private static void findAllPaths(ArrayList<Edge>[] graph, int src, int dest, boolean[] visited, String path) {
		if(src == dest) {
			System.out.println(path + src);
			return;
		}
			
		visited[src] = true;

		for (Edge edge : graph[src]) {
			if (!visited[edge.dst])
				findAllPaths(graph, edge.dst, dest, visited, path + src);
		}
		
		visited[src] = false;
	}

	@SuppressWarnings("unchecked")
	private static ArrayList<Edge>[] createGraph() {
		ArrayList<Edge>[] graph = new ArrayList[7];

		for (int i = 0; i < graph.length; i++) {
			graph[i] = new ArrayList<>();
		}

		graph[0].add(new Edge(0, 1, 10));
		graph[0].add(new Edge(0, 3, 40));

		graph[1].add(new Edge(1, 0, 10));
		graph[1].add(new Edge(1, 2, 10));

		graph[2].add(new Edge(2, 1, 10));
		graph[2].add(new Edge(2, 3, 10));

		graph[3].add(new Edge(3, 0, 40));
		graph[3].add(new Edge(3, 2, 10));
		graph[3].add(new Edge(3, 4, 2));

		graph[4].add(new Edge(4, 3, 2));
		graph[4].add(new Edge(4, 5, 3));
		graph[4].add(new Edge(4, 6, 8));

		graph[5].add(new Edge(5, 4, 3));
		graph[5].add(new Edge(5, 6, 3));

		graph[6].add(new Edge(6, 4, 8));
		graph[6].add(new Edge(6, 5, 3));

		return graph;
	}

	static class Edge {
		int src;
		int dst;
		int weight;

		public Edge(int src, int dst, int weight) {
			super();
			this.src = src;
			this.dst = dst;
			this.weight = weight;
		}

		@Override
		public String toString() {
			return src + "-" + dst + "@" + weight;
		}
	}

}
