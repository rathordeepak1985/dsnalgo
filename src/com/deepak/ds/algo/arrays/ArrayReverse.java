package com.deepak.ds.algo.arrays;

import java.util.Arrays;

import static java.lang.System.*;

public class ArrayReverse {
	public static void main(String[] args) {
		reverse(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 });
		reverse(new int[] { 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 });
		reverse(new int[] { 5, -2, -7, 4, -10, 60, 70, 80, 90, 100 });
	}

	private static void reverse(int[] arr) {
		int low = 0;
		int high = arr.length - 1;
		while (high >= low) {
			int temp = arr[low];
			arr[low] = arr[high];
			arr[high] = temp;
			low++;
			high--;
		}
		out.println(Arrays.toString(arr));
	}

}
