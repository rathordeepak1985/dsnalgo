package com.deepak.ds.algo.arrays;

import static java.lang.System.*;

public class FindElementInArray {

	public static void main(String[] args) {
		findElement(new int[] { 5, 2, 7, 3, 8, 1, 90 }, 90);
		findElement(new int[] { 5, 2, 7, 3, 8, 1, 90 }, 5);
		findElement(new int[] { 5, 2, 7, 3, 8, 1, 90 }, 7);
		findElement(new int[] { 5, 2, 7, 3, 8, 1, 90 }, 100);
		findElement(new int[] { 5, 2, 7, 3, 8, 1, 90 }, -100);
	}

	private static void findElement(int[] arr, int elementToFind) {
		boolean found = false;
		for (int i = 0; i < arr.length; i++) {
			if (elementToFind == arr[i]) {
				out.println("Found at index :" + i);
				found = true;
			}
		}
		if (!found) {
			out.println("Item " + elementToFind + " not present in array");
		}
	}
}
