package com.deepak.ds.algo.arrays;

import static java.lang.System.*;

public class FirstAndLastIndex {
	public static void main(String[] args) {
		findIndex(new int[] { 1, 1, 1, 2, 3, 3, 3, 3, 4, 4, 5, 5, 5, 5, 6 }, 6);
		findIndex(new int[] { 1, 1, 1, 2, 3, 3, 3, 3, 4, 4, 5, 5, 5, 5, 6 }, 1);
		findIndex(new int[] { 1, 1, 1, 2, 3, 3, 3, 3, 4, 4, 5, 5, 5, 5, 6 }, 3);

		out.println("==================================================");

		findIndexUsingBS(new int[] { 1, 1, 1, 2, 3, 3, 3, 3, 4, 4, 5, 5, 5, 5, 6 }, 6);
		//findIndexUsingBS(new int[] { 1, 1, 1, 2, 3, 3, 3, 3, 4, 4, 5, 5, 5, 5, 6 }, 1);
		//findIndexUsingBS(new int[] { 1, 1, 1, 2, 3, 3, 3, 3, 4, 4, 5, 5, 5, 5, 6 }, 3);
	}

	private static void findIndexUsingBS(int[] arr, int num) {
		int l = 0;
		int r = arr.length - 1;
		int fi = -1;
		int li = arr.length - 1;
		if (r > l) {
			int mid = l + (r - l) / 2;
			if (arr[mid] == num) {
				fi = mid;
				li = mid;
			}
			if (arr[mid] > num) {
				fi = binarySearch(arr, l, mid - 1, num);
			}
			if (arr[mid] < num) {
				li = binarySearch(arr, mid + 1, r, num);
			}
		}
		
	}

	private static void findIndex(int[] arr, int num) {
		int fi = -1;
		int li = Integer.MAX_VALUE;
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] == num && fi == -1)
				fi = i;
			if (arr[i] == num && fi > -1)
				li = i;

		}
		out.println("First Index :" + fi + " Last Index :" + li);
	}

	private static int binarySearch(int[] arr, int l, int r, int num) {
		int fi = -1;
		int li = arr.length - 1;
		
		if (r >= l) {
			int mid = l + (r - l) / 2;
			if (arr[mid] == num) {
				return mid;
			}
			if (arr[mid] > num) {
				binarySearch(arr, l, mid - 1, num);
			}
			if (arr[mid] < num) {
				binarySearch(arr, mid + 1, r, num);
			}
		}
		out.println("First Index :" + fi + " Last Index :" + li);
		return -1;
	}
}
