package com.deepak.ds.algo.arrays;

public class BarChart {
	public static void main(String[] args) {
		barChart(new int[] { 20, 10, 5, 12, 17 });
	}

	private static void barChart(int[] arr) {
		int y = arr[arr.length - 1];
		int x = arr.length - 1;
		for (int i = y; i <= 0; i--) {
			for (int j = 0; j <= x; j++) {
				if (y <= arr[j]) {
					System.out.println("*");
				}
			}
		}
	}
}
