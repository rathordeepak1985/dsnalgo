package com.deepak.ds.algo.arrays;

import static java.lang.System.*;

public class ArraySpan {
	public static void main(String[] args) {
		int spanValue = spanOfArray(new int[] { 20, 15, 90, 59, 5, 16, 100, 55 });
		out.println("Span of array is :" + spanValue);
	}

	private static int spanOfArray(int[] arr) {
		int max = Integer.MIN_VALUE;
		int min = Integer.MAX_VALUE;
		for (int i = 0; i < arr.length; i++) {
			if (arr[i] > max)
				max = arr[i];
			if (arr[i] < min)
				min = arr[i];

		}
		return max - min;
	}
}
