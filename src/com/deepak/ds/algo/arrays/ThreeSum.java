package com.deepak.ds.algo.arrays;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ThreeSum {
    public static void main(String[] args) {
        List<List<Integer>> result = threeSum(new int[]{-1,0,1,2,-1,-4});
        System.out.println(result);
    }

    private static List<List<Integer>> threeSum(int[] nums) {
        List<List<Integer>> result = new ArrayList<>();
        for (int i = 0; i < nums.length-1 ; i++) {
            int num1 = i;
            int num2 = i+1;
            int num3 = nums.length-1;

            while (num3 > num2){
                int sum = num1 + num2 + num3;
                if(sum == 0) {
                    Arrays.asList(num1, num2, num3);
                    num3--;
                }else if (sum > 0){
                    num3--;
                }else
                    num2++;
            }



        }
        return result;
    }
}
