package com.deepak.ds.algo.sorting;

import java.util.Arrays;

public class BubbleSort {
    public static void main(String[] args) {
        System.out.println("UnSorted="+Arrays.toString(new int[] {5,9,8,2,1}));
        System.out.println("Sorted="+Arrays.toString(bubbleSort(new int[] {5,9,8,2,1})));

    }

    private static int[] bubbleSort(int[] arr) {
        for (int i = 0; i < arr.length-1; i++) {
            for (int j = 0; j < arr.length-i-1 ; j++) {
             if(arr[j] > arr[j+1])
                 swap(arr,j);
                System.out.print((i+1)+"."+(j+1)+"=="+Arrays.toString(arr)+"   ");
            }
            System.out.println();
        }
        return arr;
    }

    private static void swap(int[] arr, int j) {
        int temp = arr[j];
        arr[j] = arr[j+1];
        arr[j+1] = temp;
    }
}
