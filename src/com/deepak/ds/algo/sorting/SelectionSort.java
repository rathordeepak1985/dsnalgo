package com.deepak.ds.algo.sorting;

import java.util.Arrays;

public class SelectionSort {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(selectionSort(new int[] {9,8,2,1,10,6,4,3})));
    }

    private static int[] selectionSort(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            int smallest = findSmallest(arr,i);
            System.out.println(smallest);
        }
        return arr;
    }

    private static int findSmallest(int[] arr, int i) {
        int min = Integer.MAX_VALUE;
        for (int j = i; j < arr.length ; j++) {
            if(arr[j] < min)
                min = arr[j];
        }
        return min;
    }
}
