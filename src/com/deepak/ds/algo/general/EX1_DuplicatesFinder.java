package com.deepak.ds.algo.general;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class EX1_DuplicatesFinder {

    public static void main(String[] args) {
        System.out.println(findDuplicates(new int[] {1,2,3,1,2,3,1,2,3,4,4,4,4,5,1,0}));
        System.out.println(findDuplicates(new int[] {10,3,10,4,10,7,1,9,2,9,9,9,2}));
    }

    public static List<Integer> findDuplicates(int[] inputArray) {
        HashSet<Integer> set = new HashSet<>();
        Arrays.sort(inputArray);
        for (int value : inputArray) {
            set.add(value);
        }
        return new ArrayList<>(set);
    }
}
