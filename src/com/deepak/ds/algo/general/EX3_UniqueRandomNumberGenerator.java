package com.deepak.ds.algo.general;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EX3_UniqueRandomNumberGenerator {
    public static void main(String[] args) {
        System.out.println(generateUniqueRandomNumbers(5,50,100));
    }

    public static List<Integer> generateUniqueRandomNumbers(int desiredCount, int minInclusive, int maxInclusive) {
        List<Integer> result = new ArrayList<>();
        for(int i = 0 ; i < desiredCount ; i++){
            int random_int = (int)Math.floor(Math.random()*(maxInclusive-minInclusive+1)+minInclusive);
            result.add(random_int);
        }
        Collections.sort(result);
        return result;
    }
}
