package com.deepak.ds.algo.general;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class EX2_IndicesSumming {

    public static void main(String[] args) {
        final int[] inputArray = {1, 5, 2, 0, 4, 11, 9, 6, 12, 7};
        final int targetValue = 9;
        System.out.println(findIndicesMatchingTargetSum(inputArray,targetValue));
    }

    public static Set<String> findIndicesMatchingTargetSum(final int[] inputArray, int targetSum) {
        Set<String> expected = new HashSet<>();
        for(int i = 0 ; i <inputArray.length ; i++){
            int x = targetSum - inputArray[i];
            for (int j = i+1 ; j < inputArray.length ; j++){
                if(x == inputArray[j])
                    expected.add(i+","+j);
            }
        }
        return expected;
    }
}
