package com.deepak.ds.algo.recursion;

import static java.lang.System.*;

public class ArrayReversePrint {
	public static void main(String[] args) {
		print(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 }, 0);
	}

	private static void print(int[] arr, int n) {
		if (n == arr.length)
			return;
		print(arr, n + 1);
		out.println(arr[n]);

	}
}
