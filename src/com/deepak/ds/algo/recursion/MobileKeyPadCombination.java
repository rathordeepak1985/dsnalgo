package com.deepak.ds.algo.recursion;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MobileKeyPadCombination {

    private static final Map<Character,String> KEYPAD = Map.of('2',"abc",'3',"def",'4',"ghi",'5',"jkl",'6',"mno",'7',"pqrs",'8',"tuv",'9',"wxyz");

    public static void main(String[] args) {
        printKeyPadComb("25","");
        System.out.println();
        System.out.println(returnKeyPadComb("25"));
    }

    private static List<String> returnKeyPadComb(String keys) {
        if(keys.length() == 0)
            return List.of();
        return helper(keys);
    }

    private static List<String> helper(String keys) {
        if(keys.length() == 0)
            return List.of("");
        List<String> combination =  new ArrayList<>();
        for (String comb : helper(keys.substring(1))){
            String letters = KEYPAD.get(keys.charAt(0));
            for (int i = 0 ; i < letters.length() ; i++){
                combination.add(letters.charAt(i)+comb);
            }
        }
        return combination;
    }

    private static void printKeyPadComb(String keys, String comb) {
        if(keys.length() == 0){
            System.out.println(comb);
            return;
        }
        String letters = KEYPAD.get(keys.charAt(0));
        for (int i = 0 ; i < letters.length() ; i++){
            printKeyPadComb(keys.substring(1),comb+letters.charAt(i));
        }

    }
}
