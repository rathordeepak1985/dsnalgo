package com.deepak.ds.algo.recursion;

import static java.lang.System.*;

public class PowerOfN {

	public static void main(String[] args) {
		out.println(powerOfN(2, 6));
		out.println(powerOfN1(2, 6));
	}

	private static int powerOfN1(int n, int p) {
		if (p == 0)
			return 1;
		int pow = powerOfN1(n, p - 1);
		if (pow % 2 == 0)
			return n * pow;
		else
			return pow * pow * n;
	}

	private static int powerOfN(int n, int p) {
		if (p == 0)
			return 1;
		return n * powerOfN(n, p - 1);
	}

}
