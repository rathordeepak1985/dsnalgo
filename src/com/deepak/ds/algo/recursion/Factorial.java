package com.deepak.ds.algo.recursion;

public class Factorial {
	public static void main(String[] args) {
		System.out.println(factorial(5));
		System.out.println(factorial(10));
	}

	private static int factorial(int i) {
		if (i == 1)
			return 1;
		int fact = factorial(i - 1);
		return i * fact;
	}
}
