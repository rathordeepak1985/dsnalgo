package com.deepak.ds.algo.recursion;

import static java.lang.System.*;

public class ArrayPrint {
	public static void main(String[] args) {
		print(new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 0 }, 0);
	}

	private static void print(int[] arr, int n) {
		if (n == arr.length)
			return;
		out.println(arr[n]);
		print(arr, n + 1);

	}

}
