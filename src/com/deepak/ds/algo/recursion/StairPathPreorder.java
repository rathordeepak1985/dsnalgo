package com.deepak.ds.algo.recursion;

import java.util.ArrayList;
import java.util.List;

import static java.lang.System.*;

public class StairPathPreorder {
    public static void main(String[] args) {
        List<String> result = new ArrayList<>();
        stairPath(5,"");
        out.println("--------------------");
        stairPathReturn(5,"",result);
    }

    private static void stairPathReturn(int stairs, String path,List<String> result) {
        if(stairs == 0){
            out.print(path+" ");
            return;
        }
        if(stairs < 0)
            return;
        stairPathReturn(stairs-1,path+"1",result);
        stairPathReturn(stairs-2,path+"2",result);
    }


    private static void stairPath(int stairs , String path) {
        if(stairs == 0){
            out.print(path+" ");
            return;
        }
         if(stairs < 0)
             return;
        stairPath(stairs-1,path+"1");
        stairPath(stairs-2,path+"2");
    }
}
