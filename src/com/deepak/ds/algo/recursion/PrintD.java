package com.deepak.ds.algo.recursion;

import static java.lang.System.*;

public class PrintD {

	public static void main(String[] args) {
		printD(10);
	}

	private static void printD(int n) {
		if (n == 0)
			return;
		out.println(n);
		printD(n - 1);
	}

}
