package com.deepak.ds.algo.recursion;

import static java.lang.System.*;

public class PtintI {
	public static void main(String[] args) {
		printI(10);
	}

	private static void printI(int n) {
		if (n == 0)
			return;
		printI(n - 1);
		out.println(n);

	}
}
