package com.deepak.ds.algo.recursion;

import static java.lang.System.*;

public class Fabonacci {
	public static void main(String[] args) {
		out.println(fabonacci(3));
	}

	private static int fabonacci(int n) {
		if (n <= 1)
			return n;
		return fabonacci(n - 1) + fabonacci(n - 2);
	}
}
