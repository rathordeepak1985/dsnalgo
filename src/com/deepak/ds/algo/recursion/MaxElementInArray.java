package com.deepak.ds.algo.recursion;

import static java.lang.System.*;

public class MaxElementInArray {
	public static void main(String[] args) {
		out.println(printMax(new int[] { 1, 2, 3, 4, 5, 60, 7, 8, 15, 5, 3, 7 }, 0));
	}

	private static int printMax(int[] arr, int n) {
		if (n == arr.length - 1)
			return -1;
		int currMax = printMax(arr, n + 1);
		return Math.max(arr[n], currMax);
	}
}
