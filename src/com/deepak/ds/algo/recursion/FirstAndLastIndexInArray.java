package com.deepak.ds.algo.recursion;

import static java.lang.System.*;

public class FirstAndLastIndexInArray {
	public static void main(String[] args) {
		out.println(printFirstIndex(new int[] { 1, 2, 3, 4, 4, 5, 6, 6, 6, 7, 8, 9 }, 0, 4));
		out.println(printLastIndex(new int[] { 1, 2, 3, 4, 4, 5, 6, 6, 6, 7, 8, 9 }, 11, 6));
	}

	private static int printLastIndex(int[] arr, int n, int numToSearch) {
		if(n==0)
			return -1;
		else if (arr[n] == numToSearch)
			return n;
		else
			return printLastIndex(arr, n - 1, numToSearch);

	}

	private static int printFirstIndex(int[] arr, int n, int numToSearch) {
		if (arr[n] == numToSearch)
			return n;
		else
			return printFirstIndex(arr, n + 1, numToSearch);
	}
}
