package com.deepak.ds.algo.recursion;

import java.util.ArrayList;
import java.util.List;

import static java.lang.System.*;

public class StringSubsequence {

	public static void main(String[] args) {
		out.println(subsequence("ABCDE"));
	}

	private static List<String> subsequence(String str) {
		if (str.length() == 0) {
			return List.of("");
		}
		List<String> result = new ArrayList<String>();
		for (String sub : subsequence(str.substring(1))) {
			result.add(sub);
			result.add(str.charAt(0) + sub);
		}

		return result;
	}

}
