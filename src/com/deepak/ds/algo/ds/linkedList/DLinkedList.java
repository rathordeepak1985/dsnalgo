package com.deepak.ds.algo.ds.linkedList;

public class DLinkedList {

    Node head;
    Node tail;
    int size;
    static class Node {

        int data;
        Node next;

        Node(int d)
        {
            data = d;
            next = null;
        }
    }

    public static void main(String[] args) {
        DLinkedList list = new DLinkedList();
        list.addLast(10);
        list.addLast(20);
        list.addLast(30);
        list.addFirst(5);
        list.printList();
        list.getFirst();
        list.getLast();
        list.removeFirst();
        list.removeLast();
        list.addAtIndex(2,40);
        list.printList();
        
    }

    public void addAtIndex(int index, int value) {
        Node newNode = new Node(value);
        Node current= head;
        for(int i = 0 ; i < index ; i++){
            current = current.next;
            if(i == index-1) {
                Node temp = current.next;
                current.next = newNode;
                newNode.next = temp;
            }
        }
        size++;
    }

    public void removeLast() {
        if(size == 0) {
            System.out.println("List is empty");
        }else if(size == 1){
            head = tail = null;
        }else{
            Node current = head;
            for(int i = 0 ; i < size-2 ; i++) {
                if(current.next != null) {
                    current = current.next;
                }
            }
            current.next = null;
            tail = current;
        }
        size--;

    }

    public void removeFirst() {
        if(size == 0) {
            System.out.println("List is empty");
        }else if(size == 1){
            head = tail = null;
        }else{
            head = head.next;
        }
        size--;
    }

    public void getLast() {
        if(size == 0){
            System.out.println("List is empty");
        }else{
            System.out.println(tail.data);
        }
    }

    public void getFirst() {
        if(size == 0){
            System.out.println("List is empty");
        }else{
            System.out.println(head.data);
        }
    }

    public void addFirst(int value) {
        Node newNode = new Node(value);
        if(size != 0){
            newNode.next=head;
            head=newNode;
        }else{
            head = tail = newNode;
        }
        size++;
    }

    public void addLast(int data) {
        Node newNode = new Node(data);
        if(size == 0){
            head = tail = newNode;
        }else{
            tail.next = newNode;
            tail=newNode;
        }
        size++;
    }

    public void printList()
    {
        Node currNode = head;
        System.out.print("[ ");
        while (currNode != null) {
            System.out.print(currNode.data + " ");
            currNode = currNode.next;
        }
        System.out.println("]");
    }

}
