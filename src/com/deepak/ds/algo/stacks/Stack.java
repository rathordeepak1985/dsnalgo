package com.deepak.ds.algo.stacks;

import java.util.EmptyStackException;

public class Stack {
    int[] stack;
    private int capacity;
    private int size;
    private int top;

    public Stack(int capacity) {
        stack = new int[capacity];
        top = -1;
        size = 0;
    }

    public Stack(){
        stack = new int[10];
        top = -1;
        size = 0;
    }

    public static void main(String[] args) {
        Stack stack = new Stack(5);
        stack.push(10);
        stack.push(20);
        stack.push(30);
        stack.push(40);
        stack.push(50);
        System.out.println(stack.print());
        System.out.println(stack.pop());
        System.out.println(stack.print());
    }

    private int peek() {
        if(size == 0)
            throw new EmptyStackException();
        return stack[top];
    }

    private String print() {
        StringBuilder sb = new StringBuilder("[");
        for (int value : stack) {
            sb.append(String.valueOf(value)).append(",");
        }
        return sb.append("]").toString();
    }

    private int pop() {
        return stack[top--];
    }

    private void push(int data) {
        if(size == capacity-1)
            throw new StackOverflowError();
        stack[++top] = data;
        size++;
    }
}
