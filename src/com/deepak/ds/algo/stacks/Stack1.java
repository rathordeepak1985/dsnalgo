package com.deepak.ds.algo.stacks;

import java.util.Arrays;
import java.util.EmptyStackException;

public class Stack1 {

    public static void main(String[] args) {
        MyStack stack = new MyStack(5);
        stack.push(10);
        stack.push(20);
        stack.push(30);
        stack.push(40);
        stack.push(40);
        stack.push(40);
        System.out.println(stack);
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.pop());
        System.out.println(stack.peek());
    }

    static class MyStack{
        int[] stack;
        int capacity;
        int top;
        int size;

        public MyStack(){
            stack = new int[10];
            top = -1;
            size = 0;
        }

        public MyStack(int capacity){
            stack = new int[capacity];
            top = -1;
            size=0;
        }

        public void push(int val) {
            if(size == capacity-1)
                throw new StackOverflowError();
            stack[++top] = val;
            size++;
        }

        public int pop() {
            if(size == 0)
                throw new EmptyStackException();
            return stack[top--];
        }

        public int peek() {
            if(size == 0)
                throw new EmptyStackException();
            return stack[top];
        }

        @Override
        public String toString() {
            return "MyStack{" +
                    "stack=" + Arrays.toString(stack) +
                    ", capacity=" + capacity +
                    ", top=" + top +
                    ", size=" + size +
                    '}';
        }
    }
}
