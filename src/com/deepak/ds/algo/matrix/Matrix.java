package com.deepak.ds.algo.matrix;

import static java.lang.System.out;

public class Matrix {
    public static void main(String[] args) {
        int[][] arr = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
        rowWiseTraverse(arr);
        System.out.println();
        colWiseTraverse(arr);
        System.out.println();
        waveWiseTraversal(arr);
    }

    private static void colWiseTraverse(int[][] arr) {
        System.out.println("printing col wise start");
        for (int col = 0; col < arr[0].length; col++) {
            for (int row = 0; row < arr.length; row++) {
                System.out.print(arr[row][col]+"   ");
            }
            System.out.println();
        }
        System.out.println("printing col wise end");
    }

    private static void rowWiseTraverse(int[][] arr) {
        System.out.println("printing row wise start");
        for (int row = 0; row < arr.length; row++) {
            for (int col = 0; col < arr[0].length;col++){
                System.out.print(arr[row][col]+"   ");
            }
            System.out.println();
        }
        System.out.println("printing row wise end");
    }

    private static void waveWiseTraversal(int[][] arr) {
        int count = 0;
        for (int col = 0; col < arr[0].length; col++) {
            for (int row = 0; row < arr.length; row++) {
                if(count % 2 == 0) {
                    out.print(arr[row][col] + "  ");
                }else {
                    out.print(arr[arr.length-1-row][col] + "  ");
                }
            }
            count++;
            out.println("");
        }
    }
}
