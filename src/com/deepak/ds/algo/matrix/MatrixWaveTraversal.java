package com.deepak.ds.algo.matrix;

import static java.lang.System.*;

public class MatrixWaveTraversal {

	public static void main(String[] args) {
		int[][] arr = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
		waveWiseTraversal(arr);

	}

	private static void waveWiseTraversal(int[][] arr) {
		int count = 0;
		for (int col = 0; col < arr[0].length; col++) {
			for (int row = 0; row < arr.length; row++) {
				if(count % 2 == 0) {
					out.print(arr[row][col] + "  ");
				}else {
					out.print(arr[row][col] + "  ");
				}
				
			}
			count++;
			out.println("");
		}

	}

}
