package com.deepak.ds.algo.matrix;

import static java.lang.System.*;

public class MatrixColumnTraversal {

	public static void main(String[] args) {
		int[][] arr = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } };
		rowWiseTraversal(arr);
		out.println("===============");
		colWiseTraversal(arr);
	}

	private static void rowWiseTraversal(int[][] arr) {
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				out.print(arr[i][j] + "  ");
			}
			out.println("");
		}
	}

	private static void colWiseTraversal(int[][] arr) {
		for (int i = 0; i < arr[0].length; i++) {
			for (int j = 0; j < arr.length; j++) {
				out.print(arr[j][i] + "  ");
			}
			out.println("");
		}
	}
}
