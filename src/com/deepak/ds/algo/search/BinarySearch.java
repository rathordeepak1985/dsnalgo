package com.deepak.ds.algo.search;

public class BinarySearch {
	public static void main(String[] args) {
		int[] arr = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 12, 14, 16, 18, 20 };
		binarySearch(arr, 0, arr.length - 1, 7);
		binarySearch(arr, 0, arr.length - 1, 16);
		binarySearch(arr, 0, arr.length - 1, 20);
		binarySearch(arr, 0, arr.length - 1, 2);
		binarySearch(arr, 0, arr.length - 1, 100);
	}

	private static void binarySearch(int[] arr, int l, int r, int num) {
		if (r >= l) {
			int mid = l + (r - l) / 2;
			if (arr[mid] == num) {
				System.out.println("Found at :" + mid);
			}
			if (arr[mid] > num) {
				binarySearch(arr, l, mid - 1, num);
			}
			if (arr[mid] < num) {
				binarySearch(arr, mid + 1, r, num);
			}
		}
	}
}
