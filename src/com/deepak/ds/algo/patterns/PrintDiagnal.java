package com.deepak.ds.algo.patterns;

public class PrintDiagnal {
    public static void main(String[] args) {
        printVarient1(5);
        System.out.println();
        printVarient2(5);
    }

    private static void printVarient2(int n) {
        for (int i = n; i >= 1 ; i--) {
            for (int j = n; j >= 1 ; j--) {
                if(i == j)
                    System.out.print("  ");
                else
                    System.out.print("*  ");
            }
            System.out.println();
        }
    }

    private static void printVarient1(int n) {
        for (int i = 1; i <= n ; i++) {
            for (int j = 1; j <= n ; j++) {
                if(i == j)
                    System.out.print("* ");
                else
                    System.out.print("  ");
            }
            System.out.println();
        }
    }
}
