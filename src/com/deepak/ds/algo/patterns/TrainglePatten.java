package com.deepak.ds.algo.patterns;

import static java.lang.System.*;

public class TrainglePatten {
    public static void main(String[] args) {
        printTraingle(10);
        out.println();
        printReverseTraingle(10);
        out.println();
        printRightAlignedTraingle(10);
        out.println();
        printRightAlignedReverseTraingle(10);
        out.println();
        printDiamondPattern(5);
    }

    private static void printDiamondPattern(int n) {
        int space = n/2;
        int star = 1;

    }

    private static void printRightAlignedTraingle(int n) {
        for (int i = 1; i <= n; i++) {
            for(int j = n ; j >= 1 ; j--){
                if(j <= i)
                    out.print("*  ");
                else
                    out.print("   ");
            }
            out.println();
        }
    }

    private static void printRightAlignedReverseTraingle(int n) {
        for (int i = 1; i <= n; i++) {
            for(int j = 1 ; j<= n ; j++){
                if(j < i)
                    out.print("   ");
                else
                    out.print("*  ");
            }
            out.println();
        }
    }

    private static void printReverseTraingle(int n) {
        for (int i = n; i >= 0 ; i--) {
            for (int j = 1; j <= i; j++) {
                out.print("*  ");
            }
            out.println();
        }
    }

    private static void printTraingle(int n) {
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= i; j++) {
                out.print("*  ");
            }
            out.println();
        }
    }
}
