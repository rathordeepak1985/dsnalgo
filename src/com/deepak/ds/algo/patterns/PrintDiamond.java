package com.deepak.ds.algo.patterns;

public class PrintDiamond {
    public static void main(String[] args) {
        printDiamond(5);
        System.out.println();
        printInvertedDiamond(5);
    }

    private static void printInvertedDiamond(int n) {
        int space = 1;
        int star = n / 2 + 1;
        for (int i = 1; i <= n; i++) {

            for (int j = 1; j <= star; j++) {
                System.out.print("*  ");
            }
            for (int j = 1; j <= space; j++) {
                System.out.print("  ");
            }
           
            if (i <= n / 2) {
                star--;
                space += 2;
            } else {
                star++;
                space -= 2;
            }
            System.out.println();
        }

    }

    private static void printDiamond(int n) {
        int space = n / 2;
        int star = 1;
        for (int i = 1; i <= n; i++) {
            for (int j = 1; j <= space; j++) {
                System.out.print("   ");
            }
            for (int j = 1; j <= star; j++) {
                System.out.print("*  ");
            }
            if (i <= n / 2) {
                space--;
                star += 2;
            } else {
                space++;
                star -= 2;
            }
            System.out.println();
        }

    }
}
